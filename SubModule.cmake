# Copyright 2018 Ling <mailto:hyzeta@outlook.com>
# This code is licensed under GPL v2.0

# ===================================================================
# Example: Source code structure.
# -------------------------------------------------------------------
# ./
# ├── CMakeLists.txt
# ├── SubModule.cmake
# ├── bin
# │   └── main
# │       └── main.c
# ├── include
# │   └── test
# │       └── add.h
# └── lib
#     └── add
#         └── add.c
# ===================================================================

# ===================================================================
# Example: Binary files structure (after make install on Unix).
# -------------------------------------------------------------------
# ./
# ├── bin
# │   └── main
# ├── include
# │   └── test
# │       └── add.h
# └── lib
#     └── libadd.so
# -------------------------------------------------------------------
# Note: On Windows, you need to add both <bin> and <lib> to PATH.
# ===================================================================

# ===================================================================
# Example: CMakeLists.txt
# -------------------------------------------------------------------
# cmake_minimum_required(VERSION 3.12)
# project(test
#     LANGUAGES C ASM)
#
# include(SubModule.cmake)
# add_bin(TARGET_NAME main
#     LINK_TO add)
# add_lib(TARGET_NAME add)
# ===================================================================

include_directories(${PROJECT_SOURCE_DIR}/include)

get_directory_property(HAS_PARENT PARENT_DIRECTORY)
if(NOT HAS_PARENT)
    install(DIRECTORY ${PROJECT_SOURCE_DIR}/include
        DESTINATION ${CMAKE_INSTALL_PREFIX}
        PATTERN "CMakeLists.txt" EXCLUDE
        PATTERN "README.md" EXCLUDE
        PATTERN ".DS_Store" EXCLUDE)
endif()

# ===================================================================

function(add_lib)

    set(ONE_VALUE_ARGS TARGET_NAME)
    set(MULTI_VALUE_ARGS LINK_TO)
    cmake_parse_arguments(ADD_LIB
        "" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

    aux_source_directory(${PROJECT_SOURCE_DIR}/lib/${ADD_LIB_TARGET_NAME} SRC_LIST)
    add_library(${ADD_LIB_TARGET_NAME} SHARED ${SRC_LIST})

    set(INSTALL_TYPE LIBRARY)
    set(INSTALL_DIR lib)
    install(TARGETS ${ADD_LIB_TARGET_NAME}
        ${INSTALL_TYPE} DESTINATION ${INSTALL_DIR}
        COMPONENT ${ADD_LIB_TARGET_NAME})

    target_link_libraries(${ADD_LIB_TARGET_NAME} ${ADD_LIB_LINK_TO})

    if(APPLE)
        set_property(TARGET ${ADD_LIB_TARGET_NAME}
            PROPERTY INSTALL_RPATH
            "@loader_path")
    elseif(UNIX)
        set_property(TARGET ${ADD_LIB_TARGET_NAME}
            PROPERTY INSTALL_RPATH
            "$ORIGIN")
    endif()

endfunction()

# ===================================================================

function(add_bin)

    set(ONE_VALUE_ARGS TARGET_NAME)
    set(MULTI_VALUE_ARGS LINK_TO)
    cmake_parse_arguments(ADD_BIN
        "" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

    aux_source_directory(${PROJECT_SOURCE_DIR}/bin/${ADD_BIN_TARGET_NAME} SRC_LIST)
    add_executable(${ADD_BIN_TARGET_NAME} ${SRC_LIST})

    set(INSTALL_TYPE RUNTIME)
    set(INSTALL_DIR bin)
    install(TARGETS ${ADD_BIN_TARGET_NAME}
        ${INSTALL_TYPE} DESTINATION ${INSTALL_DIR}
        COMPONENT ${ADD_BIN_TARGET_NAME})

    target_link_libraries(${ADD_BIN_TARGET_NAME} ${ADD_BIN_LINK_TO})

    if(APPLE)
        set_property(TARGET ${ADD_BIN_TARGET_NAME}
            PROPERTY INSTALL_RPATH
            "@loader_path/../lib")
    elseif(UNIX)
        set_property(TARGET ${ADD_BIN_TARGET_NAME}
            PROPERTY INSTALL_RPATH
            "$ORIGIN/../lib")
    endif()

endfunction()
