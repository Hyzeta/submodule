#!/bin/bash

find ./include ./bin ./lib -name '*.[ch]' | xargs clang-format -style=file -i
