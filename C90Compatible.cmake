# Copyright 2018 Ling <mailto:hyzeta@outlook.com>
# This code is licensed under GPL v2.0

if(CMAKE_C_COMPILER_ID MATCHES "Clang|GNU")
    # warnings
    string(CONCAT CUSTOM_WARNING_FLAGS
        "-Wall -Wextra"
        " -Wmissing-prototypes -Wstrict-prototypes -Wdeclaration-after-statement -Wvla"
        " -Wwrite-strings -Wpointer-arith -Wimplicit-fallthrough -Wshadow")
    if(CMAKE_C_COMPILER_ID MATCHES "Clang")
        string(APPEND CUSTOM_WARNING_FLAGS " -Wno-newline-eof")
    endif()
    # set cflags for debug
    set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -std=c90 -pedantic ${CUSTOM_WARNING_FLAGS}")
endif()
