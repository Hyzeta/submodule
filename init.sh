#!/bin/bash

mkdir -p include
mkdir -p bin
mkdir -p lib

if [ ! -f CMakeLists.txt ]; then
    mv CMakeLists.txt.example CMakeLists.txt
fi
